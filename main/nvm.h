#ifndef _NVM_H_
#define _NVM_H_

#include <stdint.h>

//-----------------------------------------------------------------------------
typedef enum
{
    NVM_PARAM_RESET_COUNTER,
    NVM_PARAM_PUMP_SPEED_PCT,          // uint8[4], 0-100
    NVM_PARAM_PUMP_CAL_ML_PER_MINUTE,  // float[4], mL/minute
    NVM_PARAM_PUMP_DOSING_CNT_PER_DAY, // uint8[4], 0-100, # of dosings per day
    NVM_PARAM_PUMP_DOSING_VOL_PER_DAY, // Total volume dosed per day[4]
    NVM_PARAM_PUMP_LIFETIME_RUN_STATS, // pump_lifetime_stats_t[4]
    NVM_PARAM_PUMP_LAST_DOSE_TIME,     // unix timestamp[4]
    NVM_PARAM_COUNT,
} nvm_param_t;

//-----------------------------------------------------------------------------
void nvm_init(void);

void nvm_reset(void);
int32_t nvm_get_param_int32(nvm_param_t nvm_param);
float nvm_get_param_float(nvm_param_t nvm_param);

void nvm_set_param_int32(nvm_param_t nvm_param, int32_t new_val);
void nvm_set_param_float(nvm_param_t nvm_param, float new_val);

void nvm_update_param_blobs(); // No need to pass a value, blob pointers hard fixed, writes all blobs

#endif
