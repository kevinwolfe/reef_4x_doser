#ifndef _APPLICATION_H_
#define _APPLICATION_H_

void application_init(void);
void application_handle_user_button_press(void);
char const *application_get_mqtt_status_msg(void);
void application_handle_mqtt_request_msg(char *p_msg);
char const *application_get_html(const char *p_custom_header);
char const *application_post_html(const char *p_post_data);

typedef struct
{
    uint32_t run_duration_s; // Total amount of time pump has been on in seconds
    uint32_t run_count;      // Total number of times the pump has turned on/off
} pump_lifetime_stats_t;

#define PUMP_CNT 4

extern uint8_t g_pump_speed_pct[PUMP_CNT];
extern uint8_t g_pump_cal_ml_per_min[PUMP_CNT];
extern uint8_t g_pump_dosing_cnt_per_day[PUMP_CNT];
extern uint32_t g_pump_dosing_vol_ml_per_day[PUMP_CNT];
extern pump_lifetime_stats_t g_pump_stats[PUMP_CNT];
extern uint32_t g_last_dose_time_s[PUMP_CNT];

#endif
