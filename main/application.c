#include <freertos/freertos.h>
#include <freertos/task.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <stdbool.h>

#include "debug.h"
#include "nvm.h"
#include "utils.h"
#include "application.h"
#include "hardware.h"
#include "mqtt.h"
#include "wifi.h"
#include "html.h"

// Web interface:
// - Per pump:
//    Set: Pump Speed: % 0 to 100
//    Set: Manual Pump Run: x # of seconds
//    Set: calibration: enter # of mL of last manual pump run
//    Set: Dosing Rate (# of doses / day)
//    Set: Total dosing Volume / day
//    Display: current calibration (mL / second)
//    Display: dosing volume / day, dosing volume / dose
//    Display: run time / dose, run time / day
//    Display: Pump Total Run Time

#define PUMP_CNT 4

#define MANUAL_PUMP_RUN_MIN_TIME_S 1
#define MANUAL_PUMP_RUN_MAX_TIME_S 300

#define PUMP_SPEED_MIN 1
#define PUMP_SPEED_MAX 100

#define PUMP_CAL_ML_PER_MINUTE_MIN 1
#define PUMP_CAL_ML_PER_MINUTE_MAX 255

#define PUMP_DOSE_CNT_PER_DAY_MIN 0
#define PUMP_DOSE_CNT_PER_DAY_MAX 100

#define PUMP_DOSE_VOL_ML_PER_DAY_MIN 0
#define PUMP_DOSE_VOL_ML_PER_DAY_MAX 1000

uint8_t g_pump_speed_pct[PUMP_CNT];
uint8_t g_pump_cal_ml_per_min[PUMP_CNT];
uint8_t g_pump_dosing_cnt_per_day[PUMP_CNT];
uint32_t g_pump_dosing_vol_ml_per_day[PUMP_CNT];
pump_lifetime_stats_t g_pump_stats[PUMP_CNT];

uint32_t g_last_dose_time_s[PUMP_CNT]; // Unix timestamp

#define PUMP_OFF false
#define PUMP_ON true

typedef struct
{
    bool dosing_active;
    double dosing_period_s; // Number of seconds between doses
    double next_dose_time_s;
    double runtime_per_dose_s; // Amount of time the pumps run for on each dose
    double manual_run_timeout_s;

    fet_ctl_t fet_ctl; // Hardware control
    aux_io_t low_level_input;
} pump_active_config_t;

static pump_active_config_t s_active_config[4] =
    {
        [0] = {.dosing_active = false, .fet_ctl = PUMP_0, .low_level_input = PUMP_0_LOW_SIGNAL},
        [1] = {.dosing_active = false, .fet_ctl = PUMP_1, .low_level_input = PUMP_1_LOW_SIGNAL},
        [2] = {.dosing_active = false, .fet_ctl = PUMP_2, .low_level_input = PUMP_2_LOW_SIGNAL},
        [3] = {.dosing_active = false, .fet_ctl = PUMP_3, .low_level_input = PUMP_3_LOW_SIGNAL},
};

static char tmp_buffer[2][128];

static void _application_task(void *Param);
static void _print_config(void);

static void _start_manual_pump_run(uint8_t pump_idx, int duration_s);
static void _start_test_dose(uint8_t pump_idx);
static void _update_pump_speed(uint8_t pump_idx, int pump_speed_pct);
static void _update_pump_cal(uint8_t pump_idx, int cal_ml_per_min);
static void _update_pump_dosing(uint8_t pump_idx, int dosing_cnt_per_day, int dosing_vol_ml_per_day);
static void _update_active_config(uint8_t pump_idx);
static bool _reservoir_is_low(uint8_t pump_idx);

static void _set_pump_state(uint8_t pump_idx, bool on);

//-----------------------------------------------------------------------------
const char *get_formatted_timestamp_str(uint32_t timestamp)
{
    static char strftime_buf[48];

    // wait for time to be set
    time_t now = timestamp;
    struct tm timeinfo = {0};

    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%I:%M:%S %p, %x", &timeinfo);
    return strftime_buf;
}

//-----------------------------------------------------------------------------
char *get_formatted_duration_str(char *p_buffer, uint32_t duration_s)
{
    if (!duration_s)
    {
        strcpy(p_buffer, "N/A");
        return p_buffer;
    }

    uint32_t s = duration_s;

    uint32_t d = s / (24 * 60 * 60);
    s -= d * (24 * 60 * 60);

    uint16_t h = s / (60 * 60);
    s -= h * 60 * 60;

    uint16_t m = s / (60);
    s -= m * 60;

    uint16_t retv = 0;
    if (d)
        retv += sprintf(p_buffer + retv, "%li days%s", d, (h || m || s) ? ", " : "");
    if (h)
        retv += sprintf(p_buffer + retv, "%i hours%s", h, (m || s) ? ", " : "");
    if (m)
        retv += sprintf(p_buffer + retv, "%i minutes%s", m, (s) ? ", " : "");
    if (s)
        retv += sprintf(p_buffer + retv, "%li seconds", s);

    return p_buffer;
}

//-----------------------------------------------------------------------------
static void _print_config(void)
{
    // Get the now's time
    time_t now;
    time(&now);

    for (uint8_t idx = 0; idx < PUMP_CNT; idx++)
    {
        pump_active_config_t *p_config = &s_active_config[idx];

        print("Pump %i:\n", idx);
        print("\tSpeed:          %i%%\n", g_pump_speed_pct[idx]);
        print("\tCalibration:    %i mL/min\n", g_pump_cal_ml_per_min[idx]);
        print("\tDoses per day:  %i/day\n", g_pump_dosing_cnt_per_day[idx]);
        print("\tDose volume:    %i mL/day\n", g_pump_dosing_vol_ml_per_day[idx]);
        print("\tReservoir:      %s\n", _reservoir_is_low(idx) ? "low" : "filled");
        print("\tLifetime Stats: %i cycles, %i seconds\n", g_pump_stats[idx].run_count, g_pump_stats[idx].run_duration_s);

        if (g_last_dose_time_s[idx])
            print("\tLast Dose Time: %s\n", get_formatted_timestamp_str(g_last_dose_time_s[idx]));
        else
            print("\tLast Dose Time: Never\n");

        if (p_config->next_dose_time_s)
        {
            print("\tNext Dose Time: %s (in %s)\n",
                  get_formatted_timestamp_str(p_config->next_dose_time_s),
                  get_formatted_duration_str(tmp_buffer[0], now - p_config->next_dose_time_s));
        }
        else
            print("\tNext Dose Time: None scheduled\n");
    }
}

//-----------------------------------------------------------------------------
static bool _reservoir_is_low(uint8_t pump_idx)
{
    return !hardware_aux_io_is_set(s_active_config[pump_idx].low_level_input);
}

//-----------------------------------------------------------------------------
static void _update_active_config(uint8_t pump_idx)
{
    time_t now;
    time(&now);

    pump_active_config_t *p_config = &s_active_config[pump_idx];

    // If either the does cnt or the volume = 0 / day, set the whole dose to 0 / day
    if (!g_pump_dosing_cnt_per_day[pump_idx] || !g_pump_dosing_vol_ml_per_day[pump_idx])
    {
        g_pump_dosing_cnt_per_day[pump_idx] = 0;
        g_pump_dosing_vol_ml_per_day[pump_idx] = 0;

        p_config->dosing_active = false;
        p_config->dosing_period_s = 0;
        p_config->next_dose_time_s = 0;
        p_config->runtime_per_dose_s = 0;
    }
    else
    {
        p_config->dosing_active = false;

        p_config->dosing_period_s = (24.0 * 60 * 60) / g_pump_dosing_cnt_per_day[pump_idx];
        p_config->next_dose_time_s = time(&now) + p_config->dosing_period_s;
        p_config->runtime_per_dose_s = ((double)g_pump_dosing_vol_ml_per_day[pump_idx] / g_pump_dosing_cnt_per_day[pump_idx]) / ((double)g_pump_cal_ml_per_min[pump_idx] / 60.0);

        if (g_last_dose_time_s[pump_idx])
            p_config->next_dose_time_s = g_last_dose_time_s[pump_idx]; // The task will move the value to the future
        else
            p_config->next_dose_time_s = now + p_config->dosing_period_s;

        p_config->dosing_active = true;
    }
}

//-----------------------------------------------------------------------------
static void _set_pump_state(uint8_t pump_idx, bool new_state)
{
    typedef struct
    {
        bool initialized;
        bool state;
        float turn_on_time_s;
    } pump_ctx_t;
    static pump_ctx_t pump_ctx[PUMP_CNT] = {0};

    pump_ctx_t *p_ctx = &pump_ctx[pump_idx];
    pump_active_config_t *p_config = &s_active_config[pump_idx];

    if (!p_ctx->initialized)
    {
        hardware_fet_open(p_config->fet_ctl);
        p_ctx->state = PUMP_OFF;
        p_ctx->initialized = true;
    }

    float speed_pct = ((float)CLAMP(g_pump_speed_pct[pump_idx], 0, 100)) / 100;

    if ((new_state == PUMP_ON) && (p_ctx->state == PUMP_OFF))
    {
        hardware_fet_energize(p_config->fet_ctl, speed_pct);
        g_pump_stats[pump_idx].run_count++;
        p_ctx->turn_on_time_s = system_uptime_s();
    }

    if ((new_state == PUMP_OFF) && (p_ctx->state == PUMP_ON))
    {
        hardware_fet_open(p_config->fet_ctl);
        g_pump_stats[pump_idx].run_duration_s += system_uptime_s() - p_ctx->turn_on_time_s;
    }

    p_ctx->state = new_state;
}

//-----------------------------------------------------------------------------
static void _application_task(void *Param)
{
    const uint32_t task_delay_ms = 250;

    // Start with all the pumps off
    for (uint8_t idx = 0; idx < PUMP_CNT; idx++)
        _set_pump_state(idx, PUMP_OFF);

    // Don't do anything until the time is set!
    while (!wifi_ntp_time_is_set())
        delay_ms(task_delay_ms);

    for (uint8_t idx = 0; idx < PUMP_CNT; idx++)
        _update_active_config(idx);

    // All of the nvm params the application cares about are blobs, which are loaded automatically
    // No need to explicitly get values
    _print_config();

    while (1)
    {
        delay_ms(task_delay_ms);
        hardware_toggle_led();

        // Get the current time
        time_t now;
        time(&now);

        // Check Dosing State
        for (uint8_t idx = 0; idx < PUMP_CNT; idx++)
        {
            pump_active_config_t *p_config = &s_active_config[idx];

            if (p_config->manual_run_timeout_s)
            {
                if (now < p_config->manual_run_timeout_s)
                {
                    _set_pump_state(idx, PUMP_ON);
                    continue;
                }
                p_config->manual_run_timeout_s = 0;
                _set_pump_state(idx, PUMP_OFF);
                print("Manual pump done\n");
            }

            if (!p_config->dosing_active)
            {
                _set_pump_state(idx, PUMP_OFF);
                continue;
            }

            static bool printed_start_msg[PUMP_CNT] = {0};
            static bool printed_stop_msg[PUMP_CNT] = {0};

            if ((now > p_config->next_dose_time_s) &&
                (now < (p_config->next_dose_time_s + p_config->runtime_per_dose_s)))
            {
                _set_pump_state(idx, PUMP_ON);

                if (!printed_start_msg[idx])
                {
                    print("Scheduled dosing for #%i started at %s. Dosing for %0.2f seconds\n",
                          idx, get_formatted_timestamp_str(now), p_config->runtime_per_dose_s);

                    printed_start_msg[idx] = true;
                    printed_stop_msg[idx] = false;
                }

                continue;
            }

            _set_pump_state(idx, PUMP_OFF);

            // Checking for printed_start_msg stops us from printing the message at boot before any dose has actually happened
            if (printed_start_msg[idx] && !printed_stop_msg[idx])
            {
                print("Scheduled dosing for #%i finished at %s\n", idx, get_formatted_timestamp_str(now));
                printed_start_msg[idx] = false;
                printed_stop_msg[idx] = true;
            }

            // Move our next dose time forward to something reasonable
            while (p_config->next_dose_time_s < now)
                p_config->next_dose_time_s += p_config->dosing_period_s;
        }

        // Check Level Sensor State
        for (uint8_t idx = 0; idx < PUMP_CNT; idx++)
        {
            pump_active_config_t *p_config = &s_active_config[idx];
            static bool last_aux_state[PUMP_CNT];
            if (hardware_aux_io_is_set(p_config->low_level_input) != last_aux_state[idx])
            {
                last_aux_state[idx] = hardware_aux_io_is_set(p_config->low_level_input);
                print("Low level on %i: %i\n", idx, hardware_aux_io_is_set(p_config->low_level_input));
            }
        }

        // Check Level Sensor State
        static float last_mqtt_update = 0;
        static const float mqtt_update_period_s = 15;
        if ((system_uptime_s() - last_mqtt_update) > mqtt_update_period_s)
        {
            static uint32_t msg_id = 0;
            for (uint8_t idx = 0; idx < PUMP_CNT; idx++)
            {
                pump_active_config_t *p_config = &s_active_config[idx];

                snprintf((char *)tmp_buffer, sizeof(tmp_buffer),
                         "{\"message_id\": %li, "
                         "\"active\": %s, "
                         "\"doses_per_day\": %i, "
                         "\"dose_volume_per_day_ml\": %li, "
                         "\"next_dose_time_s\": %li, "
                         "\"resevour\": \"%s\"}",
                         msg_id,
                         p_config->dosing_active ? "true" : "false",
                         g_pump_dosing_cnt_per_day[idx],
                         g_pump_dosing_vol_ml_per_day[idx],
                         (uint32_t)p_config->next_dose_time_s,
                         _reservoir_is_low(idx) ? "Low" : "Filled");

                char topic[32];
                snprintf(topic, sizeof(topic), "pump_%i", idx);
                mqtt_write_topic(topic, (uint8_t *)tmp_buffer, strlen((char *)tmp_buffer));
            }

            msg_id++;
            last_mqtt_update = system_uptime_s();
        }

        // Todo: update g_last_dose_time_s evey 1/4 day
        static float last_nvs_save = 0;
        static const float nvm_save_period_s = (24 * 60 * 60 / 4);
        if ((system_uptime_s() - last_nvs_save) > nvm_save_period_s)
        {
            nvm_update_param_blobs();
            last_nvs_save = system_uptime_s();
        }
    }
}

//-----------------------------------------------------------------------------
static char html_buffer[10 * 1024] = {0};
static char return_html_header[256] = {0};
static int s_active_tab_idx = 0;

char const *application_get_html(const char *p_custom_header)
{
    char *p_buffer = html_buffer;
    uint32_t uptime_s = (uint32_t)system_uptime_usec() / (1000 * 1000);

    if (p_custom_header)
    {
        p_buffer += sprintf(p_buffer, "%s", p_custom_header);
    }

    // Write the HTML content to the file
    p_buffer += sprintf(p_buffer, "%s", html_head);
    p_buffer += sprintf(p_buffer, html_body_system_info, get_system_time_str(), get_formatted_duration_str(tmp_buffer[0], uptime_s), __DATE__, __TIME__);

    if (!wifi_ntp_time_is_set())
    {
        p_buffer += sprintf(p_buffer, "<h2>Awaiting Time Update from NTP, please refresh</h2>\n");
    }
    else
    {
        p_buffer += sprintf(p_buffer, "%s", html_body_table_start);
        for (uint8_t idx = 0; idx < PUMP_CNT; idx++)
        {
            pump_active_config_t *p_config = &s_active_config[idx];

            if (p_config->dosing_active)
            {
                float dose_volume_per_dose_ml = 0;
                if (g_pump_dosing_cnt_per_day[idx])
                    dose_volume_per_dose_ml = ((float)g_pump_dosing_vol_ml_per_day[idx]) / g_pump_dosing_cnt_per_day[idx];

                p_buffer += sprintf(p_buffer, html_body_table_row_dose,
                                    idx,
                                    g_pump_dosing_cnt_per_day[idx],
                                    get_formatted_duration_str(tmp_buffer[0], (uint32_t)p_config->dosing_period_s),
                                    g_pump_dosing_vol_ml_per_day[idx],
                                    dose_volume_per_dose_ml,
                                    p_config->runtime_per_dose_s,
                                    get_formatted_timestamp_str(p_config->next_dose_time_s),
                                    _reservoir_is_low(idx) ? "Low" : "Filled",
                                    g_pump_cal_ml_per_min[idx],
                                    g_pump_speed_pct[idx],
                                    g_pump_stats[idx].run_count,
                                    get_formatted_duration_str(tmp_buffer[1], g_pump_stats[idx].run_duration_s));
            }
            else
                p_buffer += sprintf(p_buffer, html_body_table_row_inactive,
                                    idx,
                                    _reservoir_is_low(idx) ? "Low" : "Filled",
                                    g_pump_cal_ml_per_min[idx],
                                    g_pump_speed_pct[idx],
                                    g_pump_stats[idx].run_count,
                                    get_formatted_duration_str(tmp_buffer[1], g_pump_stats[idx].run_duration_s));
        }

        p_buffer += sprintf(p_buffer, html_body_table_end);

        for (uint8_t idx = 0; idx < PUMP_CNT; idx++)
        {
            p_buffer += sprintf(p_buffer, html_body_pump_tab_header, idx, idx);
            p_buffer += sprintf(p_buffer, html_body_pump_tab_set_dosing_form, idx,
                                g_pump_dosing_cnt_per_day[idx], PUMP_DOSE_CNT_PER_DAY_MIN, PUMP_DOSE_CNT_PER_DAY_MAX,
                                g_pump_dosing_vol_ml_per_day[idx], PUMP_DOSE_VOL_ML_PER_DAY_MIN, PUMP_DOSE_VOL_ML_PER_DAY_MAX);
            p_buffer += sprintf(p_buffer, html_body_pump_tab_manual_run_form, idx, 60, MANUAL_PUMP_RUN_MIN_TIME_S, MANUAL_PUMP_RUN_MAX_TIME_S);
            p_buffer += sprintf(p_buffer, html_body_pump_tab_set_cal_form, idx, g_pump_cal_ml_per_min[idx], PUMP_CAL_ML_PER_MINUTE_MIN, PUMP_CAL_ML_PER_MINUTE_MAX);
            p_buffer += sprintf(p_buffer, html_body_pump_tab_pump_speed_form, idx, g_pump_speed_pct[idx], PUMP_SPEED_MIN, PUMP_SPEED_MAX);
            p_buffer += sprintf(p_buffer, html_body_pump_tab_actions_form, idx);

            p_buffer += sprintf(p_buffer, html_body_pump_tab_footer);
        }
    }

    p_buffer += sprintf(p_buffer, html_body_end, s_active_tab_idx);

    return html_buffer;
}

//-----------------------------------------------------------------------------
static void _start_manual_pump_run(uint8_t pump_idx, int duration_s)
{
    if (pump_idx >= PUMP_CNT)
        return;

    duration_s = CLAMP(duration_s, MANUAL_PUMP_RUN_MIN_TIME_S, MANUAL_PUMP_RUN_MAX_TIME_S);

    time_t now;
    time(&now);

    s_active_config[pump_idx].manual_run_timeout_s = now + duration_s;

    print("Manual run on Pump %i for %i seconds\n", pump_idx, duration_s);
}

//-----------------------------------------------------------------------------
static void _start_test_dose(uint8_t pump_idx)
{
    if (pump_idx >= PUMP_CNT)
        return;

    time_t now;
    time(&now);

    if (s_active_config[pump_idx].dosing_active)
    {
        s_active_config[pump_idx].next_dose_time_s = now;
        print("Test dose queued for pump %i\n", pump_idx);
    }
    else
        print("Error: dosing for %i is not active, ignoring test dose request\n", pump_idx);
}

//-----------------------------------------------------------------------------
static void _update_pump_speed(uint8_t pump_idx, int pump_speed_pct)
{
    if (pump_idx >= PUMP_CNT)
        return;

    g_pump_speed_pct[pump_idx] = CLAMP(pump_speed_pct, PUMP_SPEED_MIN, PUMP_SPEED_MAX);
    nvm_update_param_blobs();

    print("Set pump %i speed to %i\n", pump_idx, g_pump_speed_pct[pump_idx]);
}

//-----------------------------------------------------------------------------
static void _update_pump_cal(uint8_t pump_idx, int cal_ml_per_min)
{
    if (pump_idx >= PUMP_CNT)
        return;

    g_pump_cal_ml_per_min[pump_idx] = CLAMP(cal_ml_per_min, PUMP_CAL_ML_PER_MINUTE_MIN, PUMP_CAL_ML_PER_MINUTE_MAX);
    _update_active_config(pump_idx);

    nvm_update_param_blobs();

    print("Set pump %i cal to %i mL/min\n", pump_idx, g_pump_cal_ml_per_min[pump_idx]);
}

//-----------------------------------------------------------------------------
static void _update_pump_dosing(uint8_t pump_idx, int dosing_cnt_per_day, int dosing_vol_ml_per_day)
{
    if (pump_idx >= PUMP_CNT)
        return;

    g_pump_dosing_cnt_per_day[pump_idx] = CLAMP(dosing_cnt_per_day, PUMP_DOSE_CNT_PER_DAY_MIN, PUMP_DOSE_CNT_PER_DAY_MAX);
    g_pump_dosing_vol_ml_per_day[pump_idx] = CLAMP(dosing_vol_ml_per_day, PUMP_DOSE_VOL_ML_PER_DAY_MIN, PUMP_DOSE_VOL_ML_PER_DAY_MAX);
    _update_active_config(pump_idx);

    nvm_update_param_blobs();

    print("Set Pump %i dosing rate to %i, dosing vol to %i\n", pump_idx, dosing_cnt_per_day, dosing_vol_ml_per_day);
}

//-----------------------------------------------------------------------------
char const *application_post_html(const char *p_post_data)
{
    printf("App post received\n");
    printf("Received: %s\n", p_post_data);

    int pump_idx = -1, arg_val1, arg_val2;

    if (sscanf(p_post_data, "pump=%d&pump_speed=%d", &pump_idx, &arg_val1) == 2)
        _update_pump_speed(pump_idx, arg_val1);

    if (sscanf(p_post_data, "pump=%d&manual_run_s=%d", &pump_idx, &arg_val1) == 2)
        _start_manual_pump_run(pump_idx, arg_val1);

    if (sscanf(p_post_data, "pump=%d&cal_mL=%d", &pump_idx, &arg_val1) == 2)
        _update_pump_cal(pump_idx, arg_val1);

    if (sscanf(p_post_data, "pump=%d&dose_rate=%d&dose_vol=%d", &pump_idx, &arg_val1, &arg_val2) == 3)
        _update_pump_dosing(pump_idx, arg_val1, arg_val2);

    if ((sscanf(p_post_data, "pump=%d", &pump_idx) == 1) && (strstr(p_post_data, "action=test_dose") != NULL))
        _start_test_dose(pump_idx);

    if ((sscanf(p_post_data, "pump=%d", &pump_idx) == 1) && (strstr(p_post_data, "action=reset_stats") != NULL))
    {
        g_pump_stats[pump_idx].run_count = 0;
        g_pump_stats[pump_idx].run_duration_s = 0;
        nvm_update_param_blobs();
    }

    // Make sure we stay on the same tab when we reload the page
    s_active_tab_idx = pump_idx;

    sprintf(return_html_header, "<meta http-equiv=\"refresh\" content=\"0\"/>");

    return application_get_html(return_html_header);
}

//-----------------------------------------------------------------------------
char const *application_get_mqtt_status_msg(void)
{
    static char status_msg[256];
    static uint32_t status_msg_id = 0;

    char *p_msg = status_msg;
    status_msg_id++;
    p_msg += sprintf(p_msg, "{ \"message_id\":%li,", status_msg_id);
    p_msg += sprintf(p_msg, "\"uptime\":%lu,", (uint32_t)system_uptime_s());
    p_msg += sprintf(p_msg, "\"system_time\":\"%s\"", get_system_time_str());
    p_msg += sprintf(p_msg, "}");

    return status_msg;
}

//-----------------------------------------------------------------------------
void application_handle_mqtt_request_msg(char *p_msg)
{
    print("MQTT request message data: %s\n", p_msg);
}

//-----------------------------------------------------------------------------
void application_handle_user_button_press(void)
{
    _print_config();
}

//-----------------------------------------------------------------------------
void application_init(void)
{
    xTaskCreate(_application_task, "app_task", 8192, NULL, 5, NULL);
}
