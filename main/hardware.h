#ifndef _HARDWARE_H_
#define _HARDWARE_H_

#include <stdbool.h>

#define INVALID_HARDWARE_HANDLE ((uint8_t)-1)

typedef enum
{
    FET_CTL_0, PUMP_0 = FET_CTL_0,
    FET_CTL_1, PUMP_1 = FET_CTL_1,
    FET_CTL_2, PUMP_2 = FET_CTL_2,
    FET_CTL_3, PUMP_3 = FET_CTL_3,
} fet_ctl_t;

typedef enum
{
    AUX_IO_0, PUMP_0_LOW_SIGNAL = AUX_IO_0,
    AUX_IO_1, PUMP_1_LOW_SIGNAL = AUX_IO_1,
    AUX_IO_2, PUMP_2_LOW_SIGNAL = AUX_IO_2,
    AUX_IO_3, PUMP_3_LOW_SIGNAL = AUX_IO_3,
} aux_io_t;

void hardware_init();
bool hardware_user_button_pressed(void);
void hardware_turn_on_led(void);
void hardware_turn_off_led(void);
void hardware_toggle_led(void);

bool hardware_aux_io_is_set(aux_io_t aux_io);

void hardware_fet_energize(fet_ctl_t fet_ctl, float speed_pct);
void hardware_fet_open(fet_ctl_t fet_ctl);
#endif
