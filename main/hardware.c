#include <driver/gpio.h>
#include <driver/ledc.h>

#include "utils.h"
#include "hardware.h"

#define PIN_BUTTON (0)
#define PIN_LED (14)

static uint8_t s_fet_ctl_gpio[] = {
    [FET_CTL_0] = GPIO_NUM_10,
    [FET_CTL_1] = GPIO_NUM_11,
    [FET_CTL_2] = GPIO_NUM_12,
    [FET_CTL_3] = GPIO_NUM_13,
};

static uint8_t s_aux_io_gpio[] = {
    [AUX_IO_0] = GPIO_NUM_15,
    [AUX_IO_1] = GPIO_NUM_16,
    [AUX_IO_2] = GPIO_NUM_20,
    [AUX_IO_3] = GPIO_NUM_19,
};
static bool s_led_on = false;

//-----------------------------------------------------------------------------
static void _setup_pin_pwm(ledc_timer_t timer, ledc_channel_t channel, uint8_t gpio);

//-----------------------------------------------------------------------------
void hardware_init()
{
    gpio_config_t io_conf;
    //----------------------------------------------------
    // Inputs with pullup:
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pull_up_en = 1;
    io_conf.pull_down_en = 0;
    io_conf.pin_bit_mask = 0;

    // Button Pin
    io_conf.pin_bit_mask |= (1ULL << PIN_BUTTON);
    io_conf.pin_bit_mask |= (1ULL << s_aux_io_gpio[PUMP_0_LOW_SIGNAL]);
    io_conf.pin_bit_mask |= (1ULL << s_aux_io_gpio[PUMP_1_LOW_SIGNAL]);
    io_conf.pin_bit_mask |= (1ULL << s_aux_io_gpio[PUMP_2_LOW_SIGNAL]);
    io_conf.pin_bit_mask |= (1ULL << s_aux_io_gpio[PUMP_3_LOW_SIGNAL]);

    gpio_config(&io_conf);

    //----------------------------------------------------
    // Inputs without pullup:
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pull_up_en = 0;
    io_conf.pull_down_en = 0;
    io_conf.pin_bit_mask = 0;

    gpio_config(&io_conf);

    //----------------------------------------------------
    // Outputs:
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pull_up_en = 0;
    io_conf.pull_down_en = 0;
    io_conf.pin_bit_mask = 0;

    // Set the LED pin to output
    io_conf.pin_bit_mask = (1ULL << PIN_LED);

    //----------------------------------------------------
    // Set PWMs
    // Set FET Controls to outputs
    for (uint8_t idx = 0; idx < ARRAY_SIZE(s_fet_ctl_gpio); idx++)
    {
        //    io_conf.pin_bit_mask |= ( 1ULL << s_fet_ctl_gpio[idx] );
        _setup_pin_pwm(LEDC_TIMER_0 + idx, LEDC_CHANNEL_0 + idx, s_fet_ctl_gpio[idx]);
    }

    gpio_config(&io_conf);
}

//-----------------------------------------------------------------------------
static void _setup_pin_pwm(ledc_timer_t timer, ledc_channel_t channel, uint8_t gpio)
{
    ledc_channel_config_t ledc_channel = {0};
    ledc_channel.gpio_num = gpio;
    ledc_channel.speed_mode = LEDC_LOW_SPEED_MODE;
    ledc_channel.channel = channel;
    ledc_channel.intr_type = LEDC_INTR_DISABLE;
    ledc_channel.timer_sel = timer;
    ledc_channel.duty = 0;

    ledc_timer_config_t ledc_timer = {0};
    ledc_timer.speed_mode = LEDC_LOW_SPEED_MODE;
    ledc_timer.duty_resolution = LEDC_TIMER_10_BIT;
    ledc_timer.timer_num = timer;
    ledc_timer.freq_hz = 25000;

    ledc_channel_config(&ledc_channel);
    ledc_timer_config(&ledc_timer);
}

//-----------------------------------------------------------------------------
bool hardware_user_button_pressed(void)
{
    return !gpio_get_level(PIN_BUTTON); // Active low
}

//-----------------------------------------------------------------------------
void hardware_turn_on_led(void)
{
    gpio_set_level(PIN_LED, 1);
    s_led_on = true;
}

//-----------------------------------------------------------------------------
void hardware_turn_off_led(void)
{
    gpio_set_level(PIN_LED, 0);
    s_led_on = false;
}

//-----------------------------------------------------------------------------
void hardware_toggle_led(void)
{
    s_led_on = !s_led_on;
    gpio_set_level(PIN_LED, s_led_on);
}

//-----------------------------------------------------------------------------
bool hardware_aux_io_is_set(aux_io_t aux_io)
{
    return gpio_get_level(s_aux_io_gpio[aux_io]);
}

//-----------------------------------------------------------------------------
void hardware_fet_energize(fet_ctl_t fet_ctl, float speed_pct)
{
    if (fet_ctl == INVALID_HARDWARE_HANDLE)
    {
        return;
    }

    uint8_t channel_idx = LEDC_CHANNEL_0 + (fet_ctl - FET_CTL_0);

    uint32_t pwm_duty = (uint32_t)(speed_pct * ((1 << LEDC_TIMER_10_BIT) - 1));
    ledc_set_duty(LEDC_LOW_SPEED_MODE, channel_idx, pwm_duty);
    ledc_update_duty(LEDC_LOW_SPEED_MODE, channel_idx);
}

//-----------------------------------------------------------------------------
void hardware_fet_open(fet_ctl_t fet_ctl)
{
    if (fet_ctl == INVALID_HARDWARE_HANDLE)
    {
        return;
    }
    //gpio_set_level( s_fet_ctl_gpio[fet_ctl], 0 );
    hardware_fet_energize(fet_ctl, 0);
}
