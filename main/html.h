#ifndef _HTML_
#define _HTML_

const char* html_head =
"<!DOCTYPE html>\n"
"<html lang=\"en\">\n"
"<head>\n"
"<meta charset=\"UTF-8\">\n"
"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
"<title>Reef Doser 4x</title>\n"
"<style>\n"
"  body {\n"
"    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\n"
"    background-color: #f4f4f4;\n"
"    color: #333;\n"
"    padding: 20px;\n"
"    line-height: 1.6;\n"
"  }\n"
"\n"
"  h1, h2 {\n"
"    color: #005792;\n"
"  }\n"
"\n"
"  .tab {\n"
"    overflow: hidden;\n"
"    background-color: #005792;\n"
"    border-radius: 8px 8px 0 0;\n"
"    margin-top: 20px;\n"
"    width: 640px;\n"
"  }\n"
"\n"
"  .tab button {\n"
"    background-color: inherit;\n"
"    float: left;\n"
"    border: none;\n"
"    outline: none;\n"
"    cursor: pointer;\n"
"    padding: 14px 16px;\n"
"    transition: background-color 0.3s;\n"
"    border-radius: 8px 8px 0 0;\n"
"    font-size: 16px;\n"
"    color: white;\n"
"  }\n"
"\n"
"  .tab button:hover {\n"
"    background-color: #a0a0a0;\n"
"  }\n"
"\n"
"  .tab button.active {\n"
"    background-color: #707070;\n"
"  }\n"
"\n"
".tabcontent {\n"
"  display: none;\n"
"  padding: 20px;\n"
"  border: 1px solid #ccc;\n"
"  border-top: none;\n"
"  background-color: #d0d0d0;\n"
"  border-radius: 0 0 8px 8px;\n"
"  width: 600px;\n"
"}\n"
"\n"
"  table, th, td {\n"
"    border: 1px solid #ddd;\n"
"    border-collapse: collapse;\n"
"    margin-top: 20px;\n"
"    background-color: #fff;\n"
"  }\n"
"\n"
"  th, td {\n"
"    padding: 10px;\n"
"    text-align: center;\n"
"  }\n"
"\n"
"  th {\n"
"    background-color: #005792;\n"
"    color: white;\n"
"  }\n"
"\n"
"  form {\n"
"    background-color: #fff;\n"
"    padding: 20px;\n"
"    margin-top: 20px;\n"
"    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);\n"
"    border-radius: 5px;\n"
"  }\n"
"\n"
"  input[type=\"number\"], button {\n"
"    text-align: center;\n"
"    padding: 3px;\n"
"    border: 2px solid #ddd;\n"
"    border-radius: 5px;\n"
"    box-sizing: border-box;\n"
"  }\n"
"\n"
"  input[type=\"number\"] {\n"
"    width: 75px;\n"
"    position: absolute;\n"
"    left: 275px;\n"
"  }\n"
"\n"
"  button {\n"
"    background-color: #005792;\n"
"    color: white;\n"
"    border: none;\n"
"    cursor: pointer;\n"
"    padding: 10px;\n"
"    border-radius: 5px;\n"
"  }\n"
"\n"
"  button:hover {\n"
"    background-color: #a0a0a0;\n"
"  }\n"
"</style>\n"
"</head>\n";

const char* html_body_system_info =
"<body>\n"
"<h1>System Info</h1>\n"
"<p>\n"
"System Time: %s<br>\n"
"Up-time: %s<br>\n"
"Firmware Build: %s %s</p>\n";

const char* html_body_table_start =
"<h2>Active Dosing Config:</h2>\n"
"<table>\n"
"  <tr>\n"
"    <th>Pump #</th>\n"
"    <th>Dosing Rate</th>\n"
"    <th>Dosing Interval</th>\n"
"    <th>Dosing Volume<br>Per Day</th>\n"
"    <th>Dosing Volume<br>Per Dose</th>\n"
"    <th>Pump Run Time<br>Per Dose</th>\n"
"    <th>Next Dosing</th>\n"
"    <th>Reservoir</th>\n"
"    <th>Calibration</th>\n"
"    <th>Speed</th>\n"
"    <th>Lifetime<br>Dose Count</th>\n"
"    <th>Lifetime<br>Dose Time</th>\n"
"  </tr>\n";

const char* html_body_table_row_dose =
"  <tr>\n"
"    <td>%i</td>\n"             // PUMP #
"    <td>%i doses/day</td>\n"   // Dosing Rate
"    <td>%s</td>\n"             // Dosing Interval (derived from Rate)
"    <td>%i mL</td>\n"          // Dosing Volume Per Day
"    <td>%0.2f mL</td>\n"       // Dosing Volume Per Dose
"    <td>%0.2f s</td>\n"        // Pump Run time per Dose
"    <td>%s</td>\n"             // Next Dosing
"    <td>%s</td>\n"             // tReservoir state
"    <td>%i mL/min</td>\n"      // Calibration
"    <td>%i%%</td>\n"           // Speed
"    <td>%i doses</td>\n"       // Lifetime Dose Count
"    <td>%s</td>\n"             // Lifetime Dose Time
"  </tr>\n";

const char* html_body_table_row_inactive =
"  <tr>\n"
"    <td>%i</td>\n"             // PUMP #
"    <td colspan=\"6\" style=\"text-align: center;\">No dosing scheduled</td>\n"
"    <td>%s</td>\n"             // tReservoir state
"    <td>%i mL/min</td>\n"      // Calibration
"    <td>%i%%</td>\n"           // Speed
"    <td>%i doses</td>\n"       // Lifetime Dose Count
"    <td>%s</td>\n"             // Lifetime Dose Time
"  </tr>\n";

const char* html_body_table_end =
"  <!-- More rows as in your HTML content -->\n"
"</table>\n"
"\n"
"<h2>Dosing Control:</h2>\n"
"\n"
"<div class=\"tab\">\n"
"  <button class=\"tablinks\" onclick=\"openTab(event, 'Pump0')\">Pump 0</button>\n"
"  <button class=\"tablinks\" onclick=\"openTab(event, 'Pump1')\">Pump 1</button>\n"
"  <button class=\"tablinks\" onclick=\"openTab(event, 'Pump2')\">Pump 2</button>\n"
"  <button class=\"tablinks\" onclick=\"openTab(event, 'Pump3')\">Pump 3</button>\n"
"</div>\n"
"\n";

const char* html_body_pump_tab_header =
"<div id=\"Pump%i\" class=\"tabcontent\">\n"
"  <h3>Update Pump %i:</h3>\n";

const char* html_body_pump_tab_pump_speed_form =
"  <form action=\"/\" method=\"post\">\n"
"    <input type=\"hidden\" id=\"pump\" name=\"pump\" value=\"%i\">\n"
"    Pump Speed: <input name=\"pump_speed\" type=\"number\" value=\"%i\" min=\"%i\" max=\"%i\"/><br>\n"
"    <button type=\"submit\">Set Pump Speed</button>\n"
"  </form>\n";

const char* html_body_pump_tab_manual_run_form =
"  <form action=\"/\" method=\"post\">\n"
"    <input type=\"hidden\" id=\"pump\" name=\"pump\" value=\"%i\">\n"
"    Manual Run (seconds): <input name=\"manual_run_s\" type=\"number\" value=\"%i\" min=\"%i\" max=\"%i\"/><br>\n"
"    <button type=\"submit\">Run Pump</button>\n"
"  </form>\n";

const char* html_body_pump_tab_set_cal_form =
"  <form action=\"/\" method=\"post\">\n"
"    <input type=\"hidden\" id=\"pump\" name=\"pump\" value=\"%i\">\n"
"    Calibration (mL/minute): <input name=\"cal_mL\" type=\"number\" value=\"%i\" min=\"%i\" max=\"%i\"/><br>\n"
"    </button><button type=\"submit\">Set Calibration</button>\n"
"  </form>\n";

const char* html_body_pump_tab_set_dosing_form =
"  <form action=\"/\" method=\"post\">\n"
"    <input type=\"hidden\" id=\"pump\" name=\"pump\" value=\"%i\">\n"
"    Doses (Per Day): <input name=\"dose_rate\" type=\"number\" value=\"%i\" min=\"%i\" max=\"%i\"/><br>\n"
"    Daily dosing volume (mL): <input name=\"dose_vol\" type=\"number\" value=\"%i\" min=\"%i\" max=\"%i\"/><br>\n"
"    <button type=\"submit\">Update Dosing Schedule</button>\n"
"  </form>\n";

const char* html_body_pump_tab_actions_form =
"  <form action=\"/\" method=\"post\">\n"
"    <input type=\"hidden\" id=\"pump\" name=\"pump\" value=\"%i\">\n"
"    <button type=\"submit\" name=\"action\" value=\"test_dose\" />Start Test Dose</button>"
"    <button type=\"submit\" name=\"action\" value=\"reset_stats\" />Reset Stats</button>"
"  </form>\n";

const char* html_body_pump_tab_footer =
"</div>\n";


const char* html_body_end =
"</div>\n"
"\n"
"<script>\n"
"function openTab(evt, pumpName) {\n"
"  var i, tabcontent, tablinks;\n"
"  tabcontent = document.getElementsByClassName(\"tabcontent\");\n"
"  for (i = 0; i < tabcontent.length; i++) {\n"
"    tabcontent[i].style.display = \"none\";\n"
"  }\n"
"  tablinks = document.getElementsByClassName(\"tablinks\");\n"
"  for (i = 0; i < tablinks.length; i++) {\n"
"    tablinks[i].className = tablinks[i].className.replace(\" active\", \"\");\n"
"  }\n"
"  document.getElementById(pumpName).style.display = \"block\";\n"
"  evt.currentTarget.className += \" active\";\n"
"}\n"
"document.getElementsByClassName(\"tablinks\")[%i].click();\n"
"</script>\n"
"</body>\n"
"</html>";

#endif
