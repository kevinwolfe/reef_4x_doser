#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <stdbool.h>

#define PUMP_CNT    4

typedef struct
{
  uint32_t run_duration_s;  // Total amount of time pump has been on in seconds
  uint32_t run_count;       // Total number of times the pump has turned on/off
} pump_lifetime_stats_t;

#define PUMP_CNT      4

uint8_t                g_pump_speeds[PUMP_CNT] = { 1, 2, 3, 4};
float                  g_pump_cal_ml_per_s[PUMP_CNT] = { 1.1, 2.2, 3.3, 4.4};
uint8_t                g_pump_dosing_cnt_per_day[PUMP_CNT] = { 5, 6, 7, 80 };                
pump_lifetime_stats_t  g_pump_stats[PUMP_CNT] = { { 10, 11}, {12, 13}, {14, 15}, {160000000, 17} };
uint32_t               g_next_dose_time_s[PUMP_CNT] = {1200000, 1300000, 1400000, 1500000};

const char* html_head = 
"<!DOCTYPE html>\n"
"<html lang=\"en\">\n"
"<head>\n"
"<meta charset=\"UTF-8\">\n"
"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
"<title>Reef Doser 4x</title>\n"
"<style>\n"
"  body {\n"
"    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\n"
"    background-color: #f4f4f4;\n"
"    color: #333;\n"
"    padding: 20px;\n"
"    line-height: 1.6;\n"
"  }\n"
"\n"
"  h1, h2 {\n"
"    color: #005792;\n"
"  }\n"
"\n"
"  .tab {\n"
"    overflow: hidden;\n"
"    background-color: #005792;\n"
"    border-radius: 8px 8px 0 0;\n"
"    margin-top: 20px;\n"
"    width: 640px;\n"
"  }\n"
"\n"
"  .tab button {\n"
"    background-color: inherit;\n"
"    float: left;\n"
"    border: none;\n"
"    outline: none;\n"
"    cursor: pointer;\n"
"    padding: 14px 16px;\n"
"    transition: background-color 0.3s;\n"
"    border-radius: 8px 8px 0 0;\n"
"    font-size: 16px;\n"
"    color: white;\n"
"  }\n"
"\n"
"  .tab button:hover {\n"
"    background-color: #a0a0a0;\n"
"  }\n"
"\n"
"  .tab button.active {\n"
"    background-color: #707070;\n"
"  }\n"
"\n"
".tabcontent {\n"
"  display: none;\n"
"  padding: 20px;\n"
"  border: 1px solid #ccc;\n"
"  border-top: none;\n"
"  background-color: #d0d0d0;\n"
"  border-radius: 0 0 8px 8px;\n"
"  width: 600px;\n"
"}\n"
"\n"
"  table, th, td {\n"
"    border: 1px solid #ddd;\n"
"    border-collapse: collapse;\n"
"    margin-top: 20px;\n"
"    background-color: #fff;\n"
"  }\n"
"\n"
"  th, td {\n"
"    padding: 10px;\n"
"    text-align: center;\n"
"  }\n"
"\n"
"  th {\n"
"    background-color: #005792;\n"
"    color: white;\n"
"  }\n"
"\n"
"  form {\n"
"    background-color: #fff;\n"
"    padding: 20px;\n"
"    margin-top: 20px;\n"
"    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);\n"
"    border-radius: 5px;\n"
"  }\n"
"\n"
"  input[type=\"number\"], button {\n"
"    text-align: center;\n"
"    padding: 3px;\n"
"    border: 2px solid #ddd;\n"
"    border-radius: 5px;\n"
"    box-sizing: border-box;\n"
"  }\n"
"\n"
"  input[type=\"number\"] {\n"
"    width: 75px;\n"
"    position: absolute;\n"
"    left: 275px;\n"
"  }\n"
"\n"
"  button {\n"
"    background-color: #005792;\n"
"    color: white;\n"
"    border: none;\n"
"    cursor: pointer;\n"
"    padding: 10px;\n"
"    border-radius: 5px;\n"
"  }\n"
"\n"
"  button:hover {\n"
"    background-color: #a0a0a0;\n"
"  }\n"
"</style>\n"
"</head>\n";

const char* html_body_system_info = 
"<body>\n"
"<h1>System Info</h1>\n"
"<p>System Time: %s<br>Up-time: %s</p>\n"
"<h2>Active Dosing Config:</h2>\n";

const char* html_body_table_start = 
"<table>\n"
"  <tr>\n"
"    <th>Pump #</th>\n"
"    <th>Dosing Rate</th>\n"
"    <th>Dosing Interval</th>\n"
"    <th>Next Dosing</th>\n"
"    <th>Calibration</th>\n"
"    <th>Speed</th>\n"
"    <th>Lifetime<br>Dose Count</th>\n"
"    <th>Lifetime<br>Dose Time</th>\n"
"  </tr>\n";

const char* html_body_table_row = 
"  <tr>\n"
"    <td>%i</td>\n"             // PUMP #
"    <td>%i doses/day</td>\n"   //uint8_t                g_pump_dosing_cnt_per_day[PUMP_CNT];                
"    <td>%s</td>\n"             //uint8_t                g_pump_dosing_cnt_per_day[PUMP_CNT];                 
"    <td>%s</td>\n"             //uint8_t                g_next_dose_time_s[PUMP_CNT];                 
"    <td>%.02f mL/s</td>\n"     //float                  g_pump_cal_ml_per_s[PUMP_CNT]; 
"    <td>%i%%</td>\n"           //uint8_t                g_pump_speeds[PUMP_CNT];
"    <td>%i doses</td>\n"       //pump_lifetime_stats_t  g_pump_stats[PUMP_CNT]  uint32_t run_duration_s
"    <td>%s</td>\n"             //pump_lifetime_stats_t  g_pump_stats[PUMP_CNT]  uint32_t run_count
"  </tr>\n";


const char* html_body_table_end = 
"  <!-- More rows as in your HTML content -->\n"
"</table>\n"
"\n"
"<h2>Dosing Control:</h2>\n"
"\n"
"<div class=\"tab\">\n"
"  <button class=\"tablinks\" onclick=\"openTab(event, 'Pump0')\">Pump 0</button>\n"
"  <button class=\"tablinks\" onclick=\"openTab(event, 'Pump1')\">Pump 1</button>\n"
"  <button class=\"tablinks\" onclick=\"openTab(event, 'Pump2')\">Pump 2</button>\n"
"  <button class=\"tablinks\" onclick=\"openTab(event, 'Pump3')\">Pump 3</button>\n"
"</div>\n"
"\n";

const char* html_body_pump_tab = 
"<div id=\"Pump%i\" class=\"tabcontent\">\n"
"  <h3>Update Pump %i:</h3>\n"
"  <form action=\"/\" method=\"post\">\n"
"    Pump Speed: <input name=\"cal\" type=\"number\" value=\"0\" min=\"0\" max=\"100\"/><br>\n"
"    <button type=\"submit\" name=\"Action\" value=\"pump_speed_%i\">Set Pump Speed</button>\n"
"  </form>\n"
"  <form action=\"/\" method=\"post\">\n"
"    Manual Run (seconds): <input name=\"manual_run_s\" type=\"number\" value=\"0\" min=\"0\" max=\"120\"/><br>\n"
"    <button type=\"submit\" name=\"Action\" value=\"manual_run_%i\">Run Pump</button>\n"
"  </form>\n"
"  <form action=\"/\" method=\"post\">\n"
"    Calibration (mL): <input name=\"cal_mL\" type=\"number\" value=\"0\" min=\"0\" max=\"100\"/><br>\n"
"    <button type=\"submit\" name=\"Action\" value=\"pump_cal_%i\">Set Calibration</button>\n"
"  </form>\n"
"  <form action=\"/\" method=\"post\">\n"
"    Doses (Per Day): <input name=\"dose_rate\" type=\"number\" value=\"0\" min=\"0\" max=\"6\"/><br>\n"
"    Daily dosing volume (mL): <input name=\"dose_vol\" type=\"number\" value=\"0\" min=\"0\" max=\"1000\"/><br>\n"
"    <button type=\"submit\" name=\"Action\" value=\"update_schedule_%i\">Update Dosing Schedule</button>\n"
"  </form>\n"
"</div>\n";

const char* html_body_end = 
"\n"
"<script>\n"
"function openTab(evt, pumpName) {\n"
"  var i, tabcontent, tablinks;\n"
"  tabcontent = document.getElementsByClassName(\"tabcontent\");\n"
"  for (i = 0; i < tabcontent.length; i++) {\n"
"    tabcontent[i].style.display = \"none\";\n"
"  }\n"
"  tablinks = document.getElementsByClassName(\"tablinks\");\n"
"  for (i = 0; i < tablinks.length; i++) {\n"
"    tablinks[i].className = tablinks[i].className.replace(\" active\", \"\");\n"
"  }\n"
"  document.getElementById(pumpName).style.display = \"block\";\n"
"  evt.currentTarget.className += \" active\";\n"
"}\n"
"document.getElementsByClassName(\"tablinks\")[0].click();\n"
"</script>\n"
"</body>\n"
"</html>";


//-----------------------------------------------------------------------------
const char * get_system_time_str()
{
  static char strftime_buf[48];
  
  // wait for time to be set
  time_t now = 0;
  struct tm timeinfo = { 0 };

  time(&now);
  localtime_r(&now, &timeinfo);
  strftime(strftime_buf, sizeof(strftime_buf), "%I:%M %p, %x", &timeinfo);
  return strftime_buf;
}

//-----------------------------------------------------------------------------
const char * get_formatted_timestamp_str( uint32_t timestamp )
{
  static char strftime_buf[48];
  
  // wait for time to be set
  time_t now = timestamp;
  struct tm timeinfo = { 0 };

  localtime_r(&now, &timeinfo);
  strftime(strftime_buf, sizeof(strftime_buf), "%I:%M:%S %p", &timeinfo);
  return strftime_buf;
}

//-----------------------------------------------------------------------------
char * get_formatted_duration_str( char *p_buffer, uint32_t duration_s )
{
  uint32_t s = duration_s;

  uint32_t d = s/(24*60*60); 
  s -= d*(24*60*60);

  uint16_t h = s/(60*60);
  s -= h*60*60;
  
  uint16_t m = s/(60);
  s -= m*60;

  uint16_t retv = 0;
  if ( d )
    retv += sprintf( p_buffer + retv, "%i days%s", d, ( h || m || s ) ? ", " : "" );
  if ( h )
    retv += sprintf( p_buffer + retv, "%i hours%s", h, ( m || s ) ? ", " : "" );
  if ( m )
    retv += sprintf( p_buffer + retv, "%i minutes%s", m, ( s ) ? ", " : "" );
  if ( s )
    retv += sprintf( p_buffer + retv, "%i seconds", s );

  return p_buffer;
}

static char tmp[2][64];

//-----------------------------------------------------------------------------
int main() {

    // Open a file for writing. If the file does not exist, it will be created.
    FILE *file = fopen("generated_index_test4.html", "w");
    if (file == NULL) {
        printf("Error opening file!\n");
        return 1;
    }

    uint32_t uptime_s = 23412341;

    // Write the HTML content to the file
    fprintf(file, "%s", html_head);
    fprintf(file, html_body_system_info, get_system_time_str(), get_formatted_duration_str(tmp[0], uptime_s));
    fprintf(file, "%s", html_body_table_start);
    
    for (uint8_t idx = 0; idx < PUMP_CNT; idx++)
    {
      uint32_t dosing_interval_s = (24*60*60)/g_pump_dosing_cnt_per_day[idx];
      fprintf(file, html_body_table_row, 
        idx,
        g_pump_dosing_cnt_per_day[idx],
        get_formatted_duration_str(tmp[0], dosing_interval_s),
        get_formatted_timestamp_str(g_next_dose_time_s[idx]),
        g_pump_cal_ml_per_s[idx],
        g_pump_speeds[idx],
        g_pump_stats[idx].run_count,
        get_formatted_duration_str(tmp[1], g_pump_stats[idx].run_duration_s));
    }
            
    fprintf(file, "%s", html_body_table_end);
    
    for (uint8_t idx = 0; idx < PUMP_CNT; idx++)
      fprintf(file, html_body_pump_tab, idx, idx, idx, idx, idx, idx);

    fprintf(file, "%s", html_body_end);

    // Close the file
    fclose(file);

    printf("HTML file generated successfully.\n");

    return 0;
}
