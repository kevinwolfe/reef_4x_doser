pushd "%~dp0"
openocd -f interface/jlink.cfg -f target/esp32s2.cfg -c "adapter speed 10000" -c "program_esp ../build/reef_4x_doser.bin 0x10000 reset exit"
openocd -f interface/jlink.cfg -f target/esp32s2.cfg -c "adapter speed 10000" -c "init" -c "reset run" -c "exit"
popd
